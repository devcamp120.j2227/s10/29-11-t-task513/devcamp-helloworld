import { gDevcampReact, tyLeSinhVienTheoHoc } from "./info";

function App() {
  return (
    <div className="App">
      <h1>{gDevcampReact.title}</h1>
      <img src={gDevcampReact.image} alt="devcamp" width="500px"/>
      <p>Tỷ lệ sinh viên theo học: {tyLeSinhVienTheoHoc()}</p>
      <p>{tyLeSinhVienTheoHoc() >= 15 ? "Sinh viên đăng ký học nhiều" : "Sinh viên đăng ký học ít"}</p>
      <ul>
        {
          gDevcampReact.benefits.map((value, index) => {
            return <li key={index}>{value}</li>
          })
        }
      </ul>
    </div>
  );
}

export default App;
